package practice

import java.io.Closeable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scala.io.{BufferedSource, Source}

case class Bucket(min: Int, max: Int, count: Int)

case class Stats(lineCount: Int,
                 minLineLength: Int,
                 averageLineLength: Int,
                 maxLineLength: Int,
                 buckets: Seq[Bucket])


object Practice extends App {
  private val bucketSize = 10
  private val defaultFileName = "in.txt"

  private def getSource(fileName: String): BufferedSource =
    Source.fromResource(fileName)
  private def read(in: BufferedSource): Future[Iterator[String]] =
    Future(blocking(in.getLines()))

  private def printStats(stats: Stats): Unit = {
    import stats._
    println(
      s"""
         | Total line count: $lineCount
         | min: $minLineLength
         | avg: $averageLineLength
         | max: $maxLineLength
         |
         | buckets:
         |${
        buckets.map { b =>
          import b._
          s"   - $min-$max: $count"
        }.mkString("\n")
      }""".stripMargin)
  }

  /*
    Методы для разминки
   */

  def asyncWithResource[R <: Closeable, T](resource: R)(block: R => Future[T]): Future[T] =
    ???

  def asyncCountLines: Future[Int] =
    ???

  def asyncLineLengths: Future[(String, Int)] =
    ???

  def asyncTotalLength: Future[Int] =
    ???

  def countShorterThan(maxLength: Int): Future[Int] =
    ???


  /*
    Sleep sort
    https://www.quora.com/What-is-sleep-sort
   */

  def printWithDelay(delay: FiniteDuration, s: String) =
    ???

  def sleepSort: Future[Unit] =
    ???

  /*
    Calculate file statistics
   */

  def splitToBuckets(linesWithLengths: Seq[(String, Int)]): Future[Seq[Bucket]] =
    ???

  def calculateStats: Future[Stats] =
    ???


  ???
}
